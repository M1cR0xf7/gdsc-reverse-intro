#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char KEY[10] = "key15";

int main(int argc, char **argv)
{

	if (argc < 2) {
		printf("Usage: %s [Key]\n", argv[0]);
		return EXIT_FAILURE;
	}

	strcat(KEY, "hard");

	if (strcmp(argv[1], KEY) != 0) {

		printf("KEY IS WRONG!\n");
		return EXIT_FAILURE;

	}

	printf("KEY IS CORRECT\n");

	return EXIT_SUCCESS;
}
