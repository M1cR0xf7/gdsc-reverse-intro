#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define KEY_SIZE 30

const char KEY[KEY_SIZE] = {
	0x1B, 0x0D, 0x17, 0x62, 0x03, 0x10, 0x07, 0x62,
	0x05, 0x0D, 0x0D, 0x0D, 0x0D, 0x06, 0x6E, 0x62,
	0x09, 0x07, 0x07, 0x12, 0x62, 0x05, 0x0D, 0x0B,
	0x0C, 0x05, 0x62, 0x7E, 0x71, 0x00
};


char decrypt(char c, char byte)
{
	return c ^ byte;
}

bool verify_key(const char *key)
{
	char secret = 0x42;
	size_t len = strlen(key);

	if (len != KEY_SIZE) {
		return false;
	}

	for (size_t i = 0; i < KEY_SIZE; i++) {
		char decrypted_char = decrypt(KEY[i], secret);
		if (key[i] != decrypted_char) {
			return false;
		}
	}
	return true;
}


int main(int argc, char **argv)
{
	if (argc < 2) {
		printf("Usage: %s [Key]\n", argv[0]);
		return EXIT_FAILURE;
	}

	if (!verify_key(argv[1])) {
		printf("KEY IS WRONG!\n");
		return EXIT_FAILURE;
	}

	printf("KEY IS CORRECT!\n");

	return EXIT_SUCCESS;
}
