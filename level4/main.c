#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

const char KEY[28] = {0x15, 0x29, 0x28, 0x32, 0x61,
		      0x36, 0x20, 0x32, 0x61, 0x2F,
		      0x2E, 0x35, 0x61, 0x29, 0x20,
		      0x33, 0x25, 0x61, 0x28, 0x32,
		      0x61, 0x28, 0x35, 0x7E, 0x61,
		      0x39, 0x05, 0x00};

char *decrypt(const char *str, char byte)
{
	size_t len = strlen(str);
	char *result = (char *)malloc(len + 1);
	if (result == NULL) {
		fprintf(stderr, "Memory allocation failed!\n");
		exit(EXIT_FAILURE);
	}

	for (size_t i = 0; i < len; i++) {
		result[i] = str[i] ^ byte;
	}
	result[len] = '\0';
	return result;
}


bool verify_key(char *key)
{
	char *plainkey = decrypt(KEY, 0x41);
	bool equal = strcmp(key, plainkey) == 0;

	free(plainkey);

	return equal;
}

int main(int argc, char **argv)
{
	if (argc < 2) {
		printf("Usage: %s [Key]\n", argv[0]);
		return EXIT_FAILURE;
	}

	if (!verify_key(argv[1])) {
		printf("KEY IS WRONG!\n");
		return EXIT_FAILURE;
	}


	printf("KEY IS CORRECT!\n");

	return EXIT_SUCCESS;
}
