#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char **argv)
{

	const char *key = "KEY1234";

	if (argc < 2) {
		printf("Usage: %s [Key]\n", argv[0]);
		return EXIT_FAILURE;
	}


	if (strcmp(argv[1], key) != 0) {
		printf("KEY IS WRONG!\n");
		return EXIT_FAILURE;
	}

	printf("KEY IS CORRECT\n");

	return EXIT_SUCCESS;
}
