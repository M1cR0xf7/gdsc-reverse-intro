#include <iostream>
#include <string_view>
#include <algorithm>
#include <bit>
#include <ranges>
#include <optional>

constexpr std::size_t KEY_SIZE = 30;

constexpr std::array<char, KEY_SIZE> KEY = {
    0x1B, 0x0D, 0x17, 0x62, 0x03, 0x10, 0x07, 0x62,
    0x05, 0x0D, 0x0D, 0x0D, 0x0D, 0x06, 0x6E, 0x62,
    0x09, 0x07, 0x07, 0x12, 0x62, 0x05, 0x0D, 0x0B,
    0x0C, 0x05, 0x62, 0x7E, 0x71, 0x00
};

constexpr char decrypt(char c, char byte)
{
    return c ^ byte;
}

constexpr std::optional<bool> verify_key(std::string_view key)
{
    char secret = 0x42;
    if (key.size() != KEY_SIZE) {
        return std::nullopt;
    }
    return std::ranges::equal(KEY, key, [=](char a, char b) {
        return decrypt(a, secret) == b;
    });
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " [Key]\n";
        return EXIT_FAILURE;
    }

    auto result = verify_key(argv[1]);
    if (!result.has_value()) {
        std::cout << "INVALID KEY LENGTH!\n";
        return EXIT_FAILURE;
    }

    if (!(*result)) {
        std::cout << "KEY IS WRONG!\n";
        return EXIT_FAILURE;
    }

    std::cout << "KEY IS CORRECT!\n";

    return EXIT_SUCCESS;
}
